import { ActivatedRoute, Router } from '@angular/router';
import { Receta } from './../recetas.module';
import { RecetasService } from './../recetas.service';
import { Component, OnInit } from '@angular/core';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-receta-detalle',
  templateUrl: './receta-detalle.page.html',
  styleUrls: ['./receta-detalle.page.scss'],
})
export class RecetaDetallePage implements OnInit {

  recetaActual: Receta;

  constructor(
    private actiatedRoute: ActivatedRoute,
    private RecetasService: RecetasService,
    private router: Router,
    private alertCtrl: AlertController) { }

  ngOnInit() {
    this.activatedRoute.paramMap.subscribe(paramMap => {
      const param: string = 'recetaId';
      if(!paramMap.has(param)){
        //redirect
        this.router.navigate(['/recetas']);
        return;
      }
      const recetaId: number = +paramMap.get(param);
      this.recetaActual = this.RecetasService.getReceta(recetaId);
    });
    
  }
  onDeleteReceta(){
    this.alertCrtl.create({
      header: '¿Estas seguro?',
      message: '¿Realmente quieres borrar esta receta?',
      buttons: [
        { text: 'Cancelar', role: 'cancel'},
        { text: 'Eliminar', handler: ()=>{
          this.RecetasService.deleteReceta(this.recetaActual.id);
        }}
      ]
    }).then( alert => {
      alert.present();
    });
  }
}

