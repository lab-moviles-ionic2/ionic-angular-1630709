import { Receta } from './recetas.module';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class RecetasService {
  private recetas: Receta[] = [
    {id: 1, titulo:'Pizza', imageUrl:'https://www.imagup.com/wp-content/uploads/2020/01/Cauliflower-Pizza.jpg', 
    ingredientes: ['Pan','Queso', 'Tomate', 'Peperoni']}
    {id: 2, titulo:'Tacos', imageUrl:'https://i2.wp.com/wellplated.com/wp-content/uploads/2018/07/Grilled-Flank-Steak-Tacos.jpg', 
    ingredientes: ['Tortilla','Queso', 'Carne', 'Cebolla']}
  ]
  constructor() { }
  
  getAllRecetas(){
    return [...this.recetas];
  }

  getReceta(recetaId: number){
    return {...this.recetas.find(r => {
      return r.id === recetaId;
    })};
  }
}
