import { RecetasService } from './recetas.service';
import { Receta } from './recetas.module';
import { Component, OnInit } from '@angular/core';


@Component({
  selector: 'app-recetas',
  templateUrl: './recetas.page.html',
  styleUrls: ['./recetas.page.scss'],
})
export class RecetasPage implements OnInit {

  recetas: Receta[] = [
    {id: 1, titulo:'Pizza', imageUrl:'https://www.imagup.com/wp-content/uploads/2020/01/Cauliflower-Pizza.jpg', 
    ingredientes: ['Pan','Queso', 'Tomate', 'Peperoni']}
    {id: 2, titulo:'Tacos', imageUrl:'https://i2.wp.com/wellplated.com/wp-content/uploads/2018/07/Grilled-Flank-Steak-Tacos.jpg', 
    ingredientes: ['Tortilla','Queso', 'Carne', 'Cebolla']}
  ]
  //constructor() { }
  recetas: Receta[];

  constructor (private recetasService: RecetasService) { }

  ngOnInit() {
    //this.recetas = this.recetasService.getAllRecetas();
    console.log('ngOnInit');
  }

  ionViewWillEnter(){
    console.log('ionViewWillEnter');
  }

  ionViewDidEnter(){
    console.log('ionViewDidEnter');
  }

  ionViewWillLeave(){
    console.log('ionViewWillLeave');
  }

  ionViewDidLeave(){
    console.log('ionViewDidLeave');
  }

  ngOnDestroy(){
    console.log('ngOnDestroy');
  }

}
